import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BrandsComponent }      from './brands/component';

const appRoutes: Routes = [
  {
    path: 'brands',
    component: BrandsComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
