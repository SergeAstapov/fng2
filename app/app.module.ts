import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';

import { routing } from './app.routing';

import { AppComponent }        from './app.component';
import { BrandsComponent }     from './brands/component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    routing
  ],
  declarations: [
    AppComponent,
    BrandsComponent
  ],
  providers: [

  ],
  bootstrap: [ AppComponent ]
})

export class AppModule {

}
