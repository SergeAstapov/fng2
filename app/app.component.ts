import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
    <h1>Page title</h1>
    <a [routerLink]="['/brands']">Brands</a>
    <router-outlet></router-outlet>
  `
})
export class AppComponent {
  title = 'Tour of Heroes';
}
